#include <stdio.h>
#include "esp_system.h"
#include "hal/i2c_types.h"
#include "soc/gpio_num.h"
#include "freertos/FreeRTOS.h"
#include "freertos/projdefs.h"
#include "portmacro.h"
#include "hd44780_i2c.h"

#define SDA_PIN  GPIO_NUM_19
#define SCL_PIN  GPIO_NUM_18
#define I2C_PORT I2C_NUM_0

void lcd_demo(void *args)
{
    hd44780_handle_t lcd = (hd44780_handle_t)args;
    char            *s1  = "Hekkl";
    char            *s2  = "llo\nworld!";

    uint8_t custom_char1[8] = {0x1F, 0x1F, 0x11, 0x11, 0x15, 0x11, 0x11, 0x00};
    uint8_t custom_char2[8] = {0x00, 0x11, 0x15, 0x11, 0x11, 0x11, 0x1F, 0x1F};

    hd44780_upload_char(lcd, custom_char1, 0, HD44780_F5X8); // upload 1st custom character
    hd44780_upload_char(lcd, custom_char2, 1, HD44780_F5X8); // upload 2nd custom character

    while (1) {
        hd44780_write_char(lcd, 0);                 // print 1st custom character
        hd44780_set_position(lcd, 0, 15);           // set cursor position on LCD

        hd44780_write_char(lcd, 0);
        hd44780_set_position(lcd, 1, 0);
        hd44780_write_char(lcd, 1);                 // print 2nd custom character
        hd44780_set_position(lcd, 1, 15);
        hd44780_write_char(lcd, 1);
        hd44780_set_position(lcd, 0, 4);
        hd44780_print(lcd, "LCD Demo", true, true); // write string to LCD output
        vTaskDelay(pdMS_TO_TICKS(2000));
        hd44780_clear(lcd);                         // clear LCD contents and return cursor to upper left corner

        hd44780_ctrl_cursor(lcd, true);             // enable underscore cursor
        hd44780_ctrl_cursorblink(lcd, true);        // enable blinking cursor
        vTaskDelay(pdMS_TO_TICKS(3000));
        for (char *p = s1; *p; p++) {
            hd44780_write_char(lcd, *p);            // write single character to LCD output
            vTaskDelay(pdMS_TO_TICKS(250));
        }
        vTaskDelay(pdMS_TO_TICKS(1000));

        for (int i = 0; i < 3; i++) {
            hd44780_move_cursor(lcd, HD44780_MOVE_LEFT, 1,
                                false);             // move cursor on LCD (would also work when invisible)
            vTaskDelay(pdMS_TO_TICKS(500));
        }
        vTaskDelay(pdMS_TO_TICKS(1000));

        for (char *p = s2; *p; p++) {
            hd44780_write_char(lcd, *p);
            vTaskDelay(pdMS_TO_TICKS(250));
        }
        vTaskDelay(pdMS_TO_TICKS(2000));

        hd44780_ctrl_cursor(lcd, false);            // disable underscore cursor
        hd44780_ctrl_cursorblink(lcd, false);       // disable blinking cursor
        uint8_t direction = 1;
        for (int i = 0; i < 4; i++) {
            direction = !direction;
            for (int i = 0; i < 10; i++) {
                hd44780_shift_display(lcd, (direction ? HD44780_MOVE_LEFT : HD44780_MOVE_RIGHT), 1); // shift display
                vTaskDelay(pdMS_TO_TICKS(300));
            }
        }
        hd44780_clear(lcd);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void app_main(void)
{
    hd44780_conf_t conf = {
        .sda_pin  = SDA_PIN,
        .scl_pin  = SCL_PIN,
        .i2c_port = I2C_PORT,
        // use defaults for other configuration variables
    };
    hd44780_handle_t lcd_handle = hd44780_create(&conf);

    printf("starting HD44780 LCD demo ...\n");
    TaskHandle_t demo_task;
    xTaskCreate(lcd_demo, "LCD demo", 2048, (void *)lcd_handle, 1, &demo_task);

    vTaskDelay(pdMS_TO_TICKS(30000));

    printf("stopping HD44780 LCD demo ...\n");
    vTaskDelete(demo_task);
    printf("freeing HD44780 resources ...\n");
    hd44780_delete(lcd_handle);
    printf("restarting ...\n");
    fflush(stdout);
    esp_restart();
}
