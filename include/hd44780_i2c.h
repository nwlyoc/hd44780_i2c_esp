#pragma once

#include <stdint.h>
#include "driver/i2c.h"
#include "esp_err.h"
#include "soc/gpio_num.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Indication of direction for different display operations
 */
typedef enum {
    HD44780_MOVE_LEFT  = 0x00, /*!< Advance to left */
    HD44780_MOVE_RIGHT = 0x04  /*!< Advance to right */
} hd44780_move_t;

/**
 * Indication of RAM type for read operations
 */
typedef enum {
    HD44780_DDRAM = 0x80, /*!< select DDRAM */
    HD44780_CGRAM = 0x40  /*!< select CGRAM */
} hd44780_ram_t;

/**
 * Indication of font type; normally 2 line displays can display 2 lines with 5x8 font or one line with 5x10 font
 */
typedef enum {
    HD44780_F5X8  = 8, /*!< 5x8 pixel characters */
    HD44780_F5X10 = 10 /*!< 5x10 pixel characters */
} hd44780_font_t;

/**
 * Configuration of I2C and display
 *
 * Values are copied, struct doesn't need to persist after `hd44780_create()` is called.
 */
typedef struct {
    gpio_num_t sda_pin;                   /*!< I2C SDA pin (no default, must be specified) */
    gpio_num_t scl_pin;                   /*!< I2C SCL pin (no default, must be specified) */
    i2c_port_t i2c_port;                  /*!< I2C port (no default, must be specified) */
    uint32_t   i2c_clk_speed;             /*!< I2C clock speed (maximum: 100 kHz; default: 100 kHz) */
    uint32_t   i2c_timeout;               /*!< I2C timeout (default: 1000 ms) */
    uint8_t    display_lines;             /*!< number of lines on display (default: 2) */
    uint8_t    display_columns;           /*!< number of physical columns on display (default: 16)*/
    uint8_t    display_max_char_per_line; /*!< number of maximum (including virtual) columns on display (default: 40)*/
    uint8_t   *display_lines_start_addr;  /*!< array of starting addresses for each line (default: { 0x00, 0x40 }) */
    hd44780_font_t display_font;          /*!< font type (default: HD44780_F5X8) */
} hd44780_conf_t;

/**
 * Handle used to address LCD device
 *
 * Returned by `hd44780_create()` and needed for every command.
 */
typedef void *hd44780_handle_t;

/**
 * @brief set up LCD and I2C configuration (via PCF8574)
 *
 * The I2C driver will be installed on the given port. Use `hd44780_delete()` to free the resources.
 *
 * @param config Pointer to configuration struct that sets I2C and LCD settings. All settings except `sda_pin`,
 * `scl_pin` and `i2c_port` have defaults and can be omitted.
 *
 * @return Handle for device. Used to address the device with all other `hd44780_*()` commands.
 */
extern hd44780_handle_t hd44780_create(hd44780_conf_t *config);

/**
 * @brief delete device and free all resources that belong to it, including I2C resources
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 *
 * @return
 *  - `ESP_OK`: device deleted successfully, resources freed
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid I2C port specified in configuration
 */
extern esp_err_t hd44780_delete(hd44780_handle_t handle);

/**
 * @brief clear display
 *
 * The display contents are cleared and the cursor is returned to the top left position (coordinates `(0, 0)`). If the
 * display had been shifted, it is returned to the unshifted position. Writing direction is set to right/increment.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_clear(hd44780_handle_t handle);

/**
 * @brief return display to home position
 *
 * The display contents are not changed and the cursor is returned to the top left position (coordinates `(0, 0)`). If
 * the display had been shifted, it is returned to the unshifted position.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_home(hd44780_handle_t handle);

/**
 * @brief control display state
 *
 * Specify if the display should be on or off.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param display `true` if the display should be on (default), `false` if it should be off.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_ctrl_display(hd44780_handle_t handle, _Bool display);

/**
 * @brief control cursor state
 *
 * Specify if the cursor (shown as underscore at current position) should be on or off. It marks the position where the
 * next write operation will take place.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param cursor `true` if the cursor should be shown, `false` if it should be hidden (default).
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_ctrl_cursor(hd44780_handle_t handle, _Bool cursor);

/**
 * @brief control blinking cursor state
 *
 * Specify if the blinking cursor (shown as blinking full field at current position) should be on or off. It marks the
 * position where the next write operation will take place.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param cursorblink `true` if the blinking cursor should be shown, `false` if it should be hidden (default).
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_ctrl_cursorblink(hd44780_handle_t handle, _Bool cursorblink);

/**
 * @brief control writing direction
 *
 * Specify if writing should take forwards or backwards.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param direction `HD44780_MOVE_RIGHT` for forward direction (default), `HD44780_MOVE_LEFT` for backward direction
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_ctrl_direction(hd44780_handle_t handle, hd44780_move_t direction);

/**
 * @brief control display shift
 *
 * Specify if the display should shift automatically when entering content that goes out of bounds of visible area. Note
 * that for `hd44780_print()` and `hd44780_move_cursor()` this will only have an effect if `visible` is set to `false`.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param shift `true` if the display should shift automatically, `false` if not (default).
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_ctrl_shift(hd44780_handle_t handle, _Bool shift);

/**
 * @brief set position for next write operation
 *
 * Specify display position (= cursor position) where next write operation will take place.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param row Row number. Numbering starts at 0 (top row). Limited to physical number of rows on display.
 * @param column Column number. Numbering starts at 0 (leftmost column). Limited to maximum number of characters per
 * line which can be more than physical columns on display (shift display with `hd44780_ctrl_shift()` or
 * `hd44780_shift_display()` to shift further columns into view).
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_set_position(hd44780_handle_t handle, uint8_t row, uint8_t column);

/**
 * @brief move cursor
 *
 * Move cursor a certain amount of steps. The cursor doesn't have to be enabled.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param direction Direction in which to move cursor.
 * @param steps Number of steps.
 * @param visible `true` if moving should only occur within originally visible area (starting at coordinates `(0, 0)`),
 * `false` if full virtual area should be taken into account.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_move_cursor(hd44780_handle_t handle, hd44780_move_t direction, uint8_t steps, _Bool visible);

/**
 * @brief shift display
 *
 * Shift display a certain amount of steps. The cursor doesn't have to be enabled.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param direction Direction in which to shift display.
 * @param steps Number of steps.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_shift_display(hd44780_handle_t handle, hd44780_move_t direction, uint8_t steps);

/**
 * @brief write character to display
 *
 * Write a single character to the display. The position for the next write will be advanced forward (or backward, if
 * set with `hd44780_ctrl_direction()`) one place within the full virtual display area.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param character Character to write to screen. Only a limited amount of characters are available (0x0A and 0x20~0x7D
 * except 0x5C). Other characters will be displayed as `?`.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_write_char(hd44780_handle_t handle, char character);

/**
 * @brief print string to display
 *
 * Print a string to the display. The position for the next write will be advanced forward (or backward, if set with
 * `hd44780_ctrl_direction()`) accordingly within the full virtual display area or only originally visible area. Line
 * wrapping can be enabled. The string will not wrap around to the beginning of the display area. If the string doesn't
 * fit according to the specified parameters, it will be clipped.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param string String to be written. Can contain characters as specified in `hd44780_write_char()`. Other characters
 * will be displayed as `?`.
 * @param visible If `true`, only originally visible screen area will be used. If `false`, full virtual width of of
 * display is used. For long single lines that should be clipped (rather than wrapped) to the physical width of the
 * display, `true` is faster since unnecessary write operations are avoided.
 * @param wrap If `true`, strings that don't fit into one line (width depends on setting of `visible`) will be wrapped
 * into subsequent lines. Strings will not wrap after the last line around to the beginning of the display area.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_print(hd44780_handle_t handle, const char *string, _Bool visible, _Bool wrap);

/**
 * @brief read busy flag and current address counter from display
 *
 * The busy flag indicates if the device is currently busy. In this state, it doesn't accept instructions. This should
 * never be the case when using the functions of this library.
 *
 * The address counter represents the position of the next read/write operation. This library keeps track of this
 * position internally without reading it from the device.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param busy_flag Variable that will hold the busy flag. It can have the values 1 (busy) or 0 (not busy).
 * @param address_counter Variable that will hold the current address for the next read/write operation.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_read_bf_ac(hd44780_handle_t handle, uint8_t *busy_flag, uint8_t *address_counter);

/**
 * @brief read data from the display
 *
 * The display has 2 types of data storage, DDRAM and CGRAM. This function can read consecutive data that is currently
 * saved in either storage. Reading the DDRAM is equivalent to reading what is currently printed on the display.
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param data Array of size n into which the data will be read. Each element represents the data at one address.
 * @param n Number of data elements that will be read.
 * @param ram Storage type the data will be read from. Can be `HD44780_DDRAM` or `HD44780_CGRAM`.
 * @param address Address at which reading the data will start. If `n > 1`, readings will be consecutive starting at
 * this address and will wrap around to the beginning of the memory if its end is reached.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid internal parameter during I2C communication
 */
extern esp_err_t hd44780_read_data(hd44780_handle_t handle, uint8_t *data, uint8_t n, hd44780_ram_t ram,
                                   uint8_t address);

/**
 * @brief save a new character in the device's memory
 *
 * Save a custom character in the device's memory for future use. 8 Characters with 5x8 or 4 with 5x10 pixels can be
 * uploaded, they share the same memory. They can be printed with the character code `slot` (for 5x8 characters) or
 * `slot << 1` (for 5x10 characters).
 *
 * @param handle Device handle. Obtained by setting up the device with `hd44780_create()`.
 * @param bitmap Bitmap of character to save to the device which is an array of bytes where each byte represents a row,
 * top to bottom. For 5x8 characters, the 8th row is shared with the cursor. Set bits light up. Each least significant
 * bit represents the rightmost column.
 * @param slot Slot in which to save custom character. For 5x8 characters 8 slots exist, for 5x10 characters 4 slots are
 * available. Numbering starts at 0.
 * @param font_type Font type of custom character. `HD44780_F5X8` for characters that are 8 pixels high, `HD44780_F5X10`
 * for characters that are 10 pixels high.
 *
 * @return
 *  - `ESP_OK`: success
 *  - `ESP_FAIL`: command failed, possible reasons: no more heap memory, device hasn't responded to command
 *  - `ESP_ERR_TIMEOUT`: I2C connection timed out, device may be blocked
 *  - `ESP_ERR_NO_MEM`: not enough memory available for I2C resources
 *  - `ESP_ERR_INVALID_STATE`: device not set up correctly
 *  - `ESP_ERR_INVALID_ARG`: invalid argument or internal parameter during I2C communication
 */
extern esp_err_t hd44780_upload_char(hd44780_handle_t handle, const uint8_t *bitmap, uint8_t slot,
                                     hd44780_font_t font_type);

#ifdef __cplusplus
}
#endif
