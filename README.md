# HD44780 I2C driver for the ESP32
This is a driver for LCDs conforming to the HD44780 standard connected to an ESP32 microcontroller via I2C with an PCF8574 GPIO expander. It handles setting up the connection with I2C and exposes the LCD's functions through an API.

## Installation
Copy the contents of this repository into the `components` directory of your project directory and include the library with the header file `hd44780_i2c.h`. Refer to the esp-idf documentation for alternative ways of including external components.

## Usage
All functions of this library are prefixed with `hd44780_` and take an `hd44780_handle_t` object as an argument (except for `hd44780_create()`) to refer to the LCD device that has been set up with `hd44780_create()`.

Refer to `hd44780_i2c.h` for the full API documentation.

## Notes
* This library has been developed for learning purposes and has not been thoroughly tested.
* This library is thread safe (or at least I tried my best to make it so).
