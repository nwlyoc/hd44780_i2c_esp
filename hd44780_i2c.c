#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "driver/i2c.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_log.h"
#include "hal/i2c_types.h"
#include "rom/ets_sys.h"
#include "FreeRTOSConfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "hd44780_i2c.h"

/**
 * `#define`s for I2C connection defaults
 */
#define I2C_MAX_CLK_SPEED 100000U /*!< default and maximum I2C clock speed of 100 kHz */
#define I2C_TIMEOUT       1000U   /*!< default I2C timeout of 1000 ms */

/**
 * `#define`s for display characteristics defaults
 */
#define DISPL_LINES      2U    /*!< default 2 display lines */
#define DISPL_COLUMNS    16U   /*!< default 16 display columns */
#define DISPL_MAXPERLINE 40U   /*!< default maximum 40 characters per line */
#define DISPL_LINE1_ADDR 0x00U /*!< default start of 1st line at 0x00 */
#define DISPL_LINE2_ADDR 0x40U /*!< default start of 2nd line at 0x40 */

/**
 * `#define`s for LCD commands on lower 4 pins (`#define FLAG_*`) and on data pins (upper 4 pins; sent
 * as 2 nibbles of 4 bits each)
 *
 * mapping PCF8574 -> LCD
 *  - P0 -> RS
 *  - P1 -> RW
 *  - P2 -> E
 *  - P3 -> Backlight
 *  - P4 -> D4
 *  - P5 -> D5
 *  - P6 -> D6
 *  - P7 -> D7
 */
#define FLAG_EN                 0x04 /*!< enable data transfer; needs to be HIGH before transfer */
#define FLAG_R                  0x02 /*!< read */
#define FLAG_W                  0x00 /*!< write */
#define FLAG_IR                 0x00 /*!< instruction register */
#define FLAG_DR                 0x01 /*!< data register */
#define FLAG_BL                 0x08 /*!< enable backlight */
#define FLAG_NOBL               0x00 /*!< disable backlight */
#define CLEAR_DISPLAY           0x01 /*!< clear display and return to home position*/
#define RETURN_HOME             0x02 /*!< return to home position */
#define ENTRYMODE               0x04 /*!< default is decrement and no shift */
#define ENTRYMODE_INCREMENT     0x02 /*!< increment address after write */
#define ENTRYMODE_SHIFT         0x01 /*!< shift display (and cursor) after write */
#define DISPLAYCTRL             0x08 /*!< default (without other flags) is OFF */
#define DISPLAYCTRL_DISPLAY     0x04 /*!< set flag to turn ON */
#define DISPLAYCTRL_CURSOR      0x02 /*!< set flag to turn ON */
#define DISPLAYCTRL_CURSORBLINK 0x01 /*!< set flag to turn ON */
#define SET_DDRAM_ADDR          0x80 /*!< combine with address to set */
#define SET_CGRAM_ADDR          0x40 /*!< combine with address to set */
#define SHIFT                   0x10 /*!< control bit for shift instruction */
#define SHIFT_CURSOR            0x00 /*!< move cursor */
#define SHIFT_DISPLAY           0x08 /*!< shift display */
#define SHIFT_RIGHT             0x04 /*!< shift/move to right */
#define SHIFT_LEFT              0x00 /*!< shift/move to left */
#define FUNCTION_SET            0x20 /*!< default is 4 bits, 1 line, 5x8 dots */
#define FUNCTION_DL             0x10 /*!< data length: 8 bit data transfer */
#define FUNCTION_N              0x08 /*!< number of lines: 2 line display */
#define FUNCTION_F              0x04 /*!< font: 5x10 dot font */

static const char *s_tag = "hd44780_i2c";

typedef enum { AC_INC, AC_DEC, AC_VIS, AC_FULL, AC_OK, AC_FAIL, AC_LINE, AC_END } ac_t;

typedef struct hd44780_handle_t_priv {
    hd44780_conf_t    config;
    uint8_t           i2c_address;
    uint8_t           setup_done;
    uint8_t           address_counter;
    uint8_t           display_ctrl;
    uint8_t           entry_mode;
    SemaphoreHandle_t mtx;
} hd44780_handle_t_priv;

static esp_err_t hd44780_setup(hd44780_handle_t_priv *handle);
static esp_err_t hd44780_write_raw(hd44780_handle_t_priv *handle, uint8_t data);
static esp_err_t hd44780_write(hd44780_handle_t_priv *handle, uint8_t data, uint8_t register_select);
static esp_err_t hd44780_scan_i2c_address(hd44780_handle_t_priv *handle);
static esp_err_t hd44780_initialize(hd44780_handle_t_priv *handle);
static ac_t      address_counter_linebreak(hd44780_handle_t_priv *handle, ac_t full_vis);
static ac_t      address_counter_change(hd44780_handle_t_priv *handle, ac_t inc_dec, ac_t full_vis);
static uint8_t   process_char(uint8_t c);
static esp_err_t hd44780_read_raw(hd44780_handle_t_priv *handle, uint8_t *data);

static esp_err_t hd44780_write_raw(hd44780_handle_t_priv *handle, uint8_t data)
{
    i2c_cmd_handle_t cmd_handle;
    esp_err_t        ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    ESP_GOTO_ON_FALSE((cmd_handle = i2c_cmd_link_create()), ESP_ERR_NO_MEM, error, s_tag,
                      "couldn't allocate space for I2C command handle");
    ESP_GOTO_ON_ERROR(i2c_master_start(cmd_handle), error, s_tag, "i2c_master_start() failed");
    ESP_GOTO_ON_ERROR(i2c_master_write_byte(cmd_handle, (handle->i2c_address << 1) | I2C_MASTER_WRITE, true), error,
                      s_tag, "i2c_master_write_byte() failed");
    // PCF8574 accepts only one data byte at a time, further bytes in same transmission are ignored
    ESP_GOTO_ON_ERROR(i2c_master_write_byte(cmd_handle, data, true), error, s_tag, "i2c_master_write_byte() failed");
    ESP_GOTO_ON_ERROR(i2c_master_stop(cmd_handle), error, s_tag, "i2c_master_stop() failed");
    ESP_GOTO_ON_ERROR(
        i2c_master_cmd_begin(handle->config.i2c_port, cmd_handle, pdMS_TO_TICKS(handle->config.i2c_timeout)), error,
        s_tag, "i2c_master_cmd_begin() failed");
    xSemaphoreGiveRecursive(handle->mtx);
    i2c_cmd_link_delete(cmd_handle);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    i2c_cmd_link_delete(cmd_handle);
    return ret;
}

static esp_err_t hd44780_write(hd44780_handle_t_priv *handle, uint8_t data, uint8_t register_select)
{
    ESP_RETURN_ON_FALSE(FLAG_IR == register_select || FLAG_DR == register_select, ESP_FAIL, s_tag, "invalid register");
    uint8_t command_base = register_select | FLAG_W | FLAG_BL;
    // LCD data pins are mapped to pin 5~8 on PCF8574; data in 4 bit mode needs to be MSB first
    uint8_t command_nibbles[2] = {
        (data & 0xF0) | command_base,
        (data << 4) | command_base,
    };
    // pulse enable flag (PCF8574 latches pin state, so this makes sure that one pulse per nibble is sent)
    uint8_t commands[] = {
        command_nibbles[0] | FLAG_EN,
        command_nibbles[0],
        command_nibbles[1] | FLAG_EN,
        command_nibbles[1],
    };
    esp_err_t ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    for (int i = 0; i < 4; i++) {
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle, commands[i]), error, s_tag, "hd44780_write_raw() failed");
    }
    xSemaphoreGiveRecursive(handle->mtx);
    // most LCD instructions take 37 us to execute; can be ignored since at 100 kHz (PCF8574's max I2C freq) I2C clock
    // period is 10 us, so transmission of a single byte will take >90 us and before first enable pulse reaches the LCD,
    // an address byte needs to be transferred to the PCF854
    // commands that take longer to execute block in their respective functions
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    return ret;
}

static esp_err_t hd44780_read_raw(hd44780_handle_t_priv *handle, uint8_t *data)
{
    i2c_cmd_handle_t cmd_handle;
    esp_err_t        ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    ESP_GOTO_ON_FALSE((cmd_handle = i2c_cmd_link_create()), ESP_ERR_NO_MEM, error, s_tag,
                      "couldn't allocate space for I2C command handle");
    ESP_GOTO_ON_ERROR(i2c_master_start(cmd_handle), error, s_tag, "i2c_master_start() failed");
    ESP_GOTO_ON_ERROR(i2c_master_write_byte(cmd_handle, (handle->i2c_address << 1) | I2C_MASTER_READ, true), error,
                      s_tag, "i2c_master_write_byte() failed");
    ESP_GOTO_ON_ERROR(i2c_master_read_byte(cmd_handle, data, true), error, s_tag, "i2c_master_read_byte() failed");
    ESP_GOTO_ON_ERROR(i2c_master_stop(cmd_handle), error, s_tag, "i2c_master_stop() failed");
    ESP_GOTO_ON_ERROR(i2c_master_cmd_begin(handle->config.i2c_port, cmd_handle, pdMS_TO_TICKS(I2C_TIMEOUT)), error,
                      s_tag, "i2c_master_cmd_begin() failed");
    i2c_cmd_link_delete(cmd_handle);
    xSemaphoreGiveRecursive(handle->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    i2c_cmd_link_delete(cmd_handle);
    return ret;
}

static esp_err_t hd44780_setup(hd44780_handle_t_priv *handle)
{
    esp_err_t ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    i2c_config_t i2c_conf = {
        .mode             = I2C_MODE_MASTER,
        .sda_io_num       = handle->config.sda_pin,
        .scl_io_num       = handle->config.scl_pin,
        .sda_pullup_en    = GPIO_PULLUP_ENABLE,
        .scl_pullup_en    = GPIO_PULLUP_ENABLE,
        .master.clk_speed = handle->config.i2c_clk_speed,
        .clk_flags        = 0,
    };
    ESP_RETURN_ON_ERROR(i2c_param_config(handle->config.i2c_port, &i2c_conf), s_tag, "i2c_param_config() failed");
    ESP_RETURN_ON_ERROR(i2c_set_data_mode(handle->config.i2c_port, I2C_DATA_MODE_MSB_FIRST, I2C_DATA_MODE_MSB_FIRST),
                        s_tag, "i2c_set_data_mode() failed");
    ESP_RETURN_ON_ERROR(i2c_driver_install(handle->config.i2c_port, I2C_MODE_MASTER, 0, 0, 0), s_tag,
                        "i2c_driver_install() failed");
    ESP_GOTO_ON_ERROR(hd44780_scan_i2c_address(handle), error, s_tag, "hd44780_scan_i2c_address() failed");
    ets_delay_us(15100); // wait >15 ms after power on according to data sheet
    ESP_GOTO_ON_ERROR(hd44780_initialize(handle), error, s_tag, "hd44780_initialize() failed");
    handle->setup_done = 1;
    xSemaphoreGiveRecursive(handle->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    ESP_RETURN_ON_ERROR(i2c_driver_delete(handle->config.i2c_port), s_tag, "i2c_driver_delete() failed");
    return ret;
}

static esp_err_t hd44780_scan_i2c_address(hd44780_handle_t_priv *handle)
{
    uint8_t          address;
    i2c_cmd_handle_t cmd_handle;
    esp_err_t        ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    // depending on configuration of pins A0~A2 on  PCF8574 its I2C address can be in between 0x20 and 0x27
    for (address = 0x20; address < 0x28; address++) {
        ESP_GOTO_ON_FALSE((cmd_handle = i2c_cmd_link_create()), ESP_ERR_NO_MEM, error, s_tag,
                          "couldn't allocate space for I2C command handle");
        ESP_GOTO_ON_ERROR(i2c_master_start(cmd_handle), error, s_tag, "i2c_master_start() failed");
        ESP_GOTO_ON_ERROR(i2c_master_write_byte(cmd_handle, (address << 1) | I2C_MASTER_WRITE, true), error, s_tag,
                          "i2c_master_write_byte() failed");
        ESP_GOTO_ON_ERROR(i2c_master_stop(cmd_handle), error, s_tag, "i2c_master_stop() failed");
        ret = i2c_master_cmd_begin(handle->config.i2c_port, cmd_handle, pdMS_TO_TICKS(handle->config.i2c_timeout));
        ESP_GOTO_ON_FALSE(ret == ESP_OK || ret == ESP_FAIL, ret, error, s_tag, "i2c_master_cmd_begin() failed");
        i2c_cmd_link_delete(cmd_handle);
        if (ret == ESP_OK) {
            break;
        }
    }
    ESP_RETURN_ON_FALSE(address < 0x28, ESP_FAIL, s_tag,
                        "couldn't reach any possible address for LCD in range 0x20 to 0x27");
    handle->i2c_address = address;
    xSemaphoreGiveRecursive(handle->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    i2c_cmd_link_delete(cmd_handle);
    return ret;
}

static esp_err_t hd44780_initialize(hd44780_handle_t_priv *handle)
{
    // initializing by instructions according to data sheet (alternative to initialization by power on)
    uint8_t   command;
    uint16_t  wait_times[] = {41100, 110, 40};
    esp_err_t ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    command = FUNCTION_SET | FUNCTION_DL | FLAG_IR | FLAG_W | FLAG_BL;
    for (int i = 0; i < 3; i++) {
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle, command | FLAG_EN), error, s_tag, "hd44780_write_raw() failed");
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle, command), error, s_tag, "hd44780_write_raw() failed");
        ets_delay_us(wait_times[i]);
    }

    command = FUNCTION_SET | FLAG_IR | FLAG_W | FLAG_BL;
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle, command | FLAG_EN), error, s_tag, "hd44780_write_raw() failed");
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle, command), error, s_tag, "hd44780_write_raw() failed");
    ets_delay_us(40);

    ESP_GOTO_ON_ERROR(
        hd44780_write(handle,
                      FUNCTION_SET | FUNCTION_N | (HD44780_F5X10 == handle->config.display_font ? FUNCTION_F : 0),
                      FLAG_IR),
        error, s_tag, "hd44780_write() failed");
    ESP_GOTO_ON_ERROR(hd44780_write(handle, DISPLAYCTRL | DISPLAYCTRL_DISPLAY, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    handle->display_ctrl = DISPLAYCTRL | DISPLAYCTRL_DISPLAY;
    ESP_GOTO_ON_ERROR(hd44780_write(handle, CLEAR_DISPLAY, FLAG_IR), error, s_tag, "hd44780_write() failed");
    handle->address_counter = 0x00;
    ESP_GOTO_ON_ERROR(hd44780_write(handle, ENTRYMODE | ENTRYMODE_INCREMENT, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    handle->entry_mode = ENTRYMODE | ENTRYMODE_INCREMENT;

    xSemaphoreGiveRecursive(handle->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    return ret;
}
static ac_t address_counter_linebreak(hd44780_handle_t_priv *handle, ac_t full_vis)
{
    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    int16_t max_index = handle->config.display_lines - 1;
    uint8_t max_chars =
        (AC_FULL == full_vis ? handle->config.display_max_char_per_line : handle->config.display_columns);

    if ((ENTRYMODE_INCREMENT & handle->entry_mode) &&
        handle->address_counter >= handle->config.display_lines_start_addr[max_index]) {
        // past end, wrap to start
        if (ESP_OK != hd44780_set_position(handle, 0, 0)) {
            xSemaphoreGiveRecursive(handle->mtx);
            return AC_FAIL;
        }
        xSemaphoreGiveRecursive(handle->mtx);
        return AC_END;
    } else if (!(ENTRYMODE_INCREMENT & handle->entry_mode) &&
               handle->address_counter >= handle->config.display_lines_start_addr[0] &&
               (max_index == 0 || handle->address_counter < handle->config.display_lines_start_addr[1])) {
        // past end, wrap to start (reverse)
        if (ESP_OK != hd44780_set_position(handle, max_index, max_chars - 1)) {
            xSemaphoreGiveRecursive(handle->mtx);
            return AC_FAIL;
        }
        xSemaphoreGiveRecursive(handle->mtx);
        return AC_END;
    } else {
        for (int16_t i = max_index; i >= 0; i--) {
            if (handle->address_counter >= handle->config.display_lines_start_addr[i]) {
                if (ENTRYMODE_INCREMENT & handle->entry_mode) {
                    // set address and position to beginning of next line
                    if (ESP_OK != hd44780_set_position(handle, i + 1, 0)) {
                        xSemaphoreGiveRecursive(handle->mtx);
                        return AC_FAIL;
                    }
                } else {
                    // set address and position to end of previous line (in case of !ENTRYMODE_INCREMENT)
                    if (ESP_OK != hd44780_set_position(handle, i - 1, max_chars - 1)) {
                        xSemaphoreGiveRecursive(handle->mtx);
                        return AC_FAIL;
                    }
                }
                xSemaphoreGiveRecursive(handle->mtx);
                return AC_LINE;
            }
        }
    }

    xSemaphoreGiveRecursive(handle->mtx);
    return AC_FAIL;
}

static ac_t address_counter_change(hd44780_handle_t_priv *handle, ac_t inc_dec, ac_t full_vis)
{
    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, s_tag,
                        "LCD blocked");
    int16_t max_index = handle->config.display_lines - 1;
    uint8_t max_chars =
        (AC_FULL == full_vis ? handle->config.display_max_char_per_line : handle->config.display_columns);
    esp_err_t ret;

    ESP_GOTO_ON_FALSE(AC_INC == inc_dec || AC_DEC == inc_dec, AC_FAIL, error, s_tag, "invalid value for inc_dec");
    if (!(ENTRYMODE_INCREMENT & handle->entry_mode)) {
        // reverse direction if ENTRYMODE is set to decrement address counter on write
        inc_dec = (AC_INC == inc_dec ? AC_DEC : AC_INC);
    }
    (AC_INC == inc_dec) ? (handle->address_counter)++ : (handle->address_counter)--;
    if (inc_dec == AC_INC) {
        if (handle->address_counter >= handle->config.display_lines_start_addr[max_index] + max_chars) {
            hd44780_set_position(handle, 0, 0);
            xSemaphoreGiveRecursive(handle->mtx);
            return AC_END;
        }
        for (int16_t i = max_index - 1; i >= 0; i--) {
            if (handle->address_counter >= handle->config.display_lines_start_addr[i] + max_chars &&
                handle->address_counter < handle->config.display_lines_start_addr[i + 1]) {
                ac_t ret_lb = address_counter_linebreak(handle, full_vis);
                xSemaphoreGiveRecursive(handle->mtx);
                return ret_lb;
            }
        }
    } else {
        if (handle->address_counter == (uint8_t)(handle->config.display_lines_start_addr[0] - 1)) {
            hd44780_set_position(handle, max_index, max_chars - 1);
            xSemaphoreGiveRecursive(handle->mtx);
            return AC_END;
        }
        for (int16_t i = 1; i <= max_index; i++) {
            if (handle->address_counter < handle->config.display_lines_start_addr[i] &&
                handle->address_counter >= handle->config.display_lines_start_addr[i - 1] + max_chars) {
                hd44780_set_position(handle, i - 1, max_chars - 1);
                xSemaphoreGiveRecursive(handle->mtx);
                return AC_LINE;
            }
        }
    }
    xSemaphoreGiveRecursive(handle->mtx);
    return AC_OK;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    return ret;
}

static uint8_t process_char(uint8_t c)
{
    // character set A00 (other character set in different regions: A02)
    if (c == 0x5C) {
        return 0x3F;
    } else if (c <= 0x0F || (c >= 0x20 && c <= 0x7D) || c == '\n') {
        return c;
    } else {
        return 0x3F;
    }
}

hd44780_handle_t hd44780_create(hd44780_conf_t *config)
{
    hd44780_handle_t_priv *handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(config->sda_pin != config->scl_pin, NULL, s_tag, "SDA and SCL pins cannot be the same");

    ESP_GOTO_ON_FALSE((handle = malloc(sizeof(*handle))), ESP_ERR_NO_MEM, error, s_tag,
                      "couldn't allocate memory for control struct");
    ESP_GOTO_ON_FALSE((handle->mtx = xSemaphoreCreateRecursiveMutex()), ESP_ERR_NO_MEM, error, s_tag,
                      "couldn't create mutex");
    ESP_GOTO_ON_FALSE(xSemaphoreTakeRecursive(handle->mtx, handle->config.i2c_timeout), ESP_ERR_TIMEOUT, error, s_tag,
                      "LCD blocked");
    handle->config     = *config;
    handle->setup_done = 0;

    if (!(handle->config.i2c_clk_speed) || handle->config.i2c_clk_speed > I2C_MAX_CLK_SPEED) {
        ESP_LOGI(s_tag, "setting I2C clock speed to maximum of %u Hz", I2C_MAX_CLK_SPEED);
        handle->config.i2c_clk_speed = I2C_MAX_CLK_SPEED;
    }
    if (!(handle->config.i2c_timeout)) {
        ESP_LOGI(s_tag, "setting I2C timeout to default of %u ms", I2C_TIMEOUT);
        handle->config.i2c_timeout = I2C_TIMEOUT;
    }
    if (!(handle->config.display_lines_start_addr)) {
        ESP_LOGI(s_tag,
                 "no display start address supplied; setting default of %u lines with start addresses of 0x%x and 0x%x",
                 DISPL_LINES, DISPL_LINE1_ADDR, DISPL_LINE2_ADDR);
        handle->config.display_lines = DISPL_LINES;
        ESP_GOTO_ON_FALSE((handle->config.display_lines_start_addr = malloc(
                               sizeof(*(handle->config.display_lines_start_addr)) * handle->config.display_lines)),
                          ESP_ERR_NO_MEM, error, s_tag, "couldn't allocate memory for control struct");
        (handle->config.display_lines_start_addr)[0] = DISPL_LINE1_ADDR;
        (handle->config.display_lines_start_addr)[1] = DISPL_LINE2_ADDR;
    } else {
        handle->config.display_lines_start_addr = NULL;
        ESP_GOTO_ON_FALSE((handle->config.display_lines_start_addr = malloc(
                               sizeof(*(handle->config.display_lines_start_addr)) * handle->config.display_lines)),
                          ESP_ERR_NO_MEM, error, s_tag, "couldn't allocate memory for control struct");
        memcpy(handle->config.display_lines_start_addr, config->display_lines_start_addr,
               sizeof(*(handle->config.display_lines_start_addr)) * handle->config.display_lines);
    }
    if (!(handle->config.display_lines)) {
        ESP_LOGI(s_tag, "setting default of %u display lines", DISPL_LINES);
        handle->config.display_lines = DISPL_LINES;
    }
    if (!(handle->config.display_columns)) {
        ESP_LOGI(s_tag, "setting default of %u display columns", DISPL_COLUMNS);
        handle->config.display_columns = DISPL_COLUMNS;
    }
    if (!(handle->config.display_max_char_per_line)) {
        ESP_LOGI(s_tag, "setting default of maximum %u characters per display line", DISPL_MAXPERLINE);
        handle->config.display_max_char_per_line = DISPL_MAXPERLINE;
    }
    if (!(handle->config.display_font)) {
        ESP_LOGI(s_tag, "setting default font type of 5x%i characters", HD44780_F5X8);
        handle->config.display_font = HD44780_F5X8;
    }

    ESP_GOTO_ON_ERROR(hd44780_setup(handle), error, s_tag, "setup failed");
    xSemaphoreGiveRecursive(handle->mtx);

    return handle;

error:
    xSemaphoreGiveRecursive(handle->mtx);
    vSemaphoreDelete(handle->mtx);
    free(handle->config.display_lines_start_addr);
    free(handle);
    return NULL;
}

esp_err_t hd44780_delete(hd44780_handle_t handle)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    ESP_GOTO_ON_ERROR(i2c_driver_delete(handle_priv->config.i2c_port), error, s_tag, "i2c_driver_delete() failed");
    vSemaphoreDelete(handle_priv->mtx);
    free(handle_priv->config.display_lines_start_addr);
    free(handle_priv);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_clear(hd44780_handle_t handle)
{
    hd44780_handle_t_priv *handle_priv = handle;
    TickType_t             wait        = pdMS_TO_TICKS(2); // no execution time specified, but probably similar to HOME
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, CLEAR_DISPLAY, FLAG_IR), error, s_tag, "hd44780_write() failed");
    handle_priv->entry_mode |= ENTRYMODE_INCREMENT;
    vTaskDelay(wait > 0 ? wait : 1);
    handle_priv->address_counter = 0x00;
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_home(hd44780_handle_t handle)
{
    hd44780_handle_t_priv *handle_priv = handle;
    TickType_t             wait        = pdMS_TO_TICKS(2); // execution time is 1.52 ms according to data sheet
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, RETURN_HOME, FLAG_IR), error, s_tag, "hd44780_write() failed");
    vTaskDelay(wait > 0 ? wait : 1);
    handle_priv->address_counter = 0x00;
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_ctrl_display(hd44780_handle_t handle, _Bool display)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (display) {
        handle_priv->display_ctrl |= DISPLAYCTRL_DISPLAY;
    } else {
        handle_priv->display_ctrl &= ~DISPLAYCTRL_DISPLAY;
    }
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, handle_priv->display_ctrl, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_ctrl_cursor(hd44780_handle_t handle, _Bool cursor)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (cursor) {
        handle_priv->display_ctrl |= DISPLAYCTRL_CURSOR;
    } else {
        handle_priv->display_ctrl &= ~DISPLAYCTRL_CURSOR;
    }
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, handle_priv->display_ctrl, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_ctrl_cursorblink(hd44780_handle_t handle, _Bool cursorblink)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (cursorblink) {
        handle_priv->display_ctrl |= DISPLAYCTRL_CURSORBLINK;
    } else {
        handle_priv->display_ctrl &= ~DISPLAYCTRL_CURSORBLINK;
    }
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, handle_priv->display_ctrl, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_ctrl_direction(hd44780_handle_t handle, hd44780_move_t direction)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (HD44780_MOVE_RIGHT == direction) {
        handle_priv->entry_mode |= ENTRYMODE_INCREMENT;
    } else {
        handle_priv->entry_mode &= ~ENTRYMODE_INCREMENT;
    }
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, handle_priv->entry_mode, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_ctrl_shift(hd44780_handle_t handle, _Bool shift)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (shift) {
        handle_priv->entry_mode |= ENTRYMODE_SHIFT;
    } else {
        handle_priv->entry_mode &= ~ENTRYMODE_SHIFT;
    }
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, handle_priv->entry_mode, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_set_position(hd44780_handle_t handle, uint8_t row, uint8_t column)
{
    hd44780_handle_t_priv *handle_priv = handle;
    uint8_t                address;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    ESP_GOTO_ON_FALSE(row < handle_priv->config.display_lines || column < handle_priv->config.display_columns, ESP_FAIL,
                      error, s_tag, "position out of bounds");
    address = handle_priv->config.display_lines_start_addr[row] + column;
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SET_DDRAM_ADDR | address, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    handle_priv->address_counter = address;
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_move_cursor(hd44780_handle_t handle, hd44780_move_t direction, uint8_t steps, _Bool visible)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;
    ac_t                   ac_dir = (direction ? AC_INC : AC_DEC);
    if (!(ENTRYMODE_INCREMENT & handle_priv->entry_mode)) {
        // address_counter_change() reverses direction if !ENTRYMODE_INCREMENT, but SHIFT is not affected by this, so
        // reverse direction here in order for address_counter_change() to reverse it again into correct direction
        ac_dir = (direction ? AC_DEC : AC_INC);
    }

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    for (int i = 0; i < steps; i++) {
        ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SHIFT | SHIFT_CURSOR | direction, FLAG_IR), error, s_tag,
                          "hd44780_write() failed");
        ESP_GOTO_ON_FALSE(AC_FAIL != address_counter_change(handle_priv, ac_dir, visible ? AC_VIS : AC_FULL), ESP_FAIL,
                          error, s_tag, "address_counter_change() failed");
    }
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_shift_display(hd44780_handle_t handle, hd44780_move_t direction, uint8_t steps)
{
    hd44780_handle_t_priv *handle_priv = handle;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    for (int i = 0; i < steps; i++) {
        ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SHIFT | SHIFT_DISPLAY | direction, FLAG_IR), error, s_tag,
                          "hd44780_write() failed");
    }
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_write_char(hd44780_handle_t handle, char character)
{
    hd44780_handle_t_priv *handle_priv = handle;
    uint8_t                c           = process_char(character);
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    if (c == '\n') {
        ESP_GOTO_ON_FALSE(AC_FAIL != address_counter_linebreak(handle_priv, AC_FULL), ESP_FAIL, error, s_tag,
                          "address_counter_linebreak() failed");
    } else {
        ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, c, FLAG_DR), error, s_tag, "hd44780_write() failed");
        ESP_GOTO_ON_FALSE(AC_FAIL != address_counter_change(handle_priv, AC_INC, AC_FULL), ESP_FAIL, error, s_tag,
                          "address_counter_change() failed");
    }
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_print(hd44780_handle_t handle, const char *string, _Bool visible, _Bool wrap)
{
    hd44780_handle_t_priv *handle_priv = handle;
    uint8_t                c;
    ac_t                   vis = visible ? AC_VIS : AC_FULL;
    ac_t                   ac_ret;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");

    if (visible) {
        // start in new line in case cursor is outside of visible area
        uint8_t cursor_visible = 0;
        for (uint8_t i = 0; i < handle_priv->config.display_lines; i++) {
            if (handle_priv->address_counter >= handle_priv->config.display_lines_start_addr[i] &&
                handle_priv->address_counter <
                    handle_priv->config.display_lines_start_addr[i] + handle_priv->config.display_columns) {
                cursor_visible = 1;
                break;
            }
        }
        if (!cursor_visible) {
            ESP_GOTO_ON_FALSE(AC_FAIL != address_counter_linebreak(handle_priv, AC_VIS), ESP_FAIL, error, s_tag,
                              "address_counter_linebreak() failed");
        }
    }

    for (; *string; string++) {
        c = process_char(*string);
        if (c == '\n') {
            ac_ret = address_counter_linebreak(handle_priv, (visible ? AC_VIS : AC_FULL));
            ESP_GOTO_ON_FALSE(AC_FAIL != ac_ret, ESP_FAIL, error, s_tag, "address_counter_linebreak() failed");
            string++;
            if (AC_END == ac_ret || !*string) {
                break;
            }
            c = process_char(*string);
        }
        ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, c, FLAG_DR), error, s_tag, "hd44780_write() failed");
        ac_ret = address_counter_change(handle_priv, AC_INC, vis);
        ESP_GOTO_ON_FALSE(AC_FAIL != ac_ret, ESP_FAIL, error, s_tag, "address_counter_change() failed");
        if (AC_END == ac_ret || (AC_LINE == ac_ret && !wrap)) {
            break;
        }
    }

    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_read_bf_ac(hd44780_handle_t handle, uint8_t *busy_flag, uint8_t *address_counter)
{
    hd44780_handle_t_priv *handle_priv      = handle;
    uint8_t                read_instruction = FLAG_R | FLAG_IR | FLAG_BL;
    uint8_t                data1, data2, data_full;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    // read pin needs to be HIGH before EN pulse
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");

    // in order to read from PCF8574, all data pins need to be set HIGH first
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction | FLAG_EN | 0xF0), error, s_tag,
                      "hd44780_write_raw() failed");
    ESP_GOTO_ON_ERROR(hd44780_read_raw(handle_priv, &data1), error, s_tag, "hd44780_read_raw() failed");
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");

    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction | FLAG_EN | 0xF0), error, s_tag,
                      "hd44780_write_raw() failed");
    ESP_GOTO_ON_ERROR(hd44780_read_raw(handle_priv, &data2), error, s_tag, "hd44780_read_raw() failed");
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");

    xSemaphoreGiveRecursive(handle_priv->mtx);

    data_full = (data1 & 0xF0) | (data2 >> 4);
    if (busy_flag) {
        *busy_flag = data_full & 0x80;
    }
    if (address_counter) {
        *address_counter = data_full & 0x7F;
    }
    return ESP_OK;

error:
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_read_data(hd44780_handle_t handle, uint8_t *data, uint8_t n, hd44780_ram_t ram, uint8_t address)
{
    hd44780_handle_t_priv *handle_priv      = handle;
    uint8_t                read_instruction = FLAG_R | FLAG_DR | FLAG_BL;
    uint8_t                data1, data2;
    esp_err_t              ret;

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");
    // address setting (or cursor shift) operation needs to take place before content at address can be read
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, ram | address, FLAG_IR), error, s_tag, "hd44780_write_raw() failed");
    // read pin needs to be HIGH before EN pulse
    ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");
    while (n--) {
        // in order to read from PCF8574, all pins need to be set HIGH first
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction | FLAG_EN | 0xF0), error, s_tag,
                          "hd44780_write_raw() failed");
        ESP_GOTO_ON_ERROR(hd44780_read_raw(handle_priv, &data1), error, s_tag, "hd44780_read_raw() failed");
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");

        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction | FLAG_EN | 0xF0), error, s_tag,
                          "hd44780_write_raw() failed");
        ESP_GOTO_ON_ERROR(hd44780_read_raw(handle_priv, &data2), error, s_tag, "hd44780_read_raw() failed");
        ESP_GOTO_ON_ERROR(hd44780_write_raw(handle_priv, read_instruction), error, s_tag, "hd44780_write_raw() failed");

        *data = (data1 & 0xF0) | (data2 >> 4);
        data++;
    }
    // return to position from before (reading from data register changes address counter)
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SET_DDRAM_ADDR | handle_priv->address_counter, FLAG_IR), error, s_tag,
                      "hd44780_write_raw() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    if (ESP_OK != hd44780_write(handle_priv, SET_DDRAM_ADDR | handle_priv->address_counter, FLAG_IR)) {
        ESP_LOGE(s_tag, "restoring address counter failed");
    }
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}

esp_err_t hd44780_upload_char(hd44780_handle_t handle, const uint8_t *bitmap, uint8_t slot, hd44780_font_t font_type)
{
    hd44780_handle_t_priv *handle_priv = handle;
    uint8_t                cgram_address;
    uint8_t                rows;
    esp_err_t              ret;

    if (HD44780_F5X8 == font_type && slot < 8) {
        cgram_address = slot << 3;
        rows          = 8;
    } else if (HD44780_F5X10 == font_type && slot < 4) {
        cgram_address = slot << 4;
        rows          = 10;
    } else {
        ESP_LOGE(s_tag, "invalid slot number or font type");
        return ESP_ERR_INVALID_ARG;
    }

    ESP_RETURN_ON_FALSE(xSemaphoreTakeRecursive(handle_priv->mtx, handle_priv->config.i2c_timeout), ESP_ERR_TIMEOUT,
                        s_tag, "LCD blocked");
    ESP_GOTO_ON_FALSE(handle_priv->setup_done, ESP_ERR_INVALID_STATE, error, s_tag, "no I2C connection to LCD set up");

    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SET_CGRAM_ADDR | cgram_address, FLAG_IR), error, s_tag,
                      "hd44780_write() failed");
    for (uint8_t i = 0; i < rows; i++) {
        ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, bitmap[i], FLAG_DR), error, s_tag, "hd44780_write() failed");
    }

    // return to position from before (writing to data register changes address counter)
    ESP_GOTO_ON_ERROR(hd44780_write(handle_priv, SET_DDRAM_ADDR | handle_priv->address_counter, FLAG_IR), error, s_tag,
                      "hd44780_write_raw() failed");
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ESP_OK;

error:
    if (ESP_OK != hd44780_write(handle_priv, SET_DDRAM_ADDR | handle_priv->address_counter, FLAG_IR)) {
        ESP_LOGE(s_tag, "restoring address counter failed");
    }
    xSemaphoreGiveRecursive(handle_priv->mtx);
    return ret;
}
